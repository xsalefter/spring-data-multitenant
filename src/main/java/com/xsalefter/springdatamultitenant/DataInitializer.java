package com.xsalefter.springdatamultitenant;

import javax.persistence.EntityManager;

import com.xsalefter.springdatamultitenant.model.Customer;
import com.xsalefter.springdatamultitenant.model.Organization;

public class DataInitializer {

    private final EntityManager entityManager;

    public DataInitializer(final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    private void withDefaultTestData() {
        Organization tenant = new Organization("Lannister");
        this.entityManager.persist(tenant);

        this.entityManager.persist(new Customer("Cercei", "Lannister", tenant));
        this.entityManager.persist(new Customer("Tyrion", "Lannister", tenant));
        this.entityManager.persist(new Customer("Jaime", "Lannister", tenant));
    }

    public void withMultipleTenants() {
        this.withDefaultTestData();

        this.entityManager.persist(new Organization("Stark"));
        final Organization exes = getOrganization("Stark");

        this.entityManager.persist(new Customer("Sansa", "Stark", exes));
        this.entityManager.persist(new Customer("John", "Snow", exes));
        this.entityManager.persist(new Customer("Catelyn", "Stark", exes));
        this.entityManager.persist(new Customer("Ned", "Stark", exes));
    }

    private Organization getOrganization(final String name) {
        return this.entityManager
            .createQuery("from Organization o where o.name = :name", Organization.class)
            .setParameter("name", name).getSingleResult();
    }
}
