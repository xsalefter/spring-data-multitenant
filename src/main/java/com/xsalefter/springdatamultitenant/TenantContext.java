package com.xsalefter.springdatamultitenant;

public final class TenantContext {

    private static ThreadLocal<Long> tenant = new ThreadLocal<>();

    public static Long get() {
        return tenant.get();
    }

    public static void set(Long tenantId) {
        tenant.set(tenantId);
    }

}
