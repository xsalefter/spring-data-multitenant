package com.xsalefter.springdatamultitenant.resource;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xsalefter.springdatamultitenant.TenantContext;
import com.xsalefter.springdatamultitenant.model.Customer;
import com.xsalefter.springdatamultitenant.repository.CustomerRepository;

@RestController
@RequestMapping("/api")
public class CustomerResource {

    private final CustomerRepository customerRepository;

    public CustomerResource(final CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @GetMapping("/customers")
    public ResponseEntity<Iterable<Customer>> get() {
        TenantContext.set(2L);
        return new ResponseEntity<Iterable<Customer>>(this.customerRepository.findAll(), HttpStatus.OK);
    }
}
