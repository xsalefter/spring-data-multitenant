package com.xsalefter.springdatamultitenant.repository;

import static org.springframework.data.jpa.repository.query.QueryUtils.toOrders;

import java.io.Serializable;
import java.util.List;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.util.Assert;

import com.xsalefter.springdatamultitenant.TenantContext;
import com.xsalefter.springdatamultitenant.model.MultiTenantCapable;

/**
 * @see SimpleJpaRepository
 */
public class MultiTenantRepositoryImpl<T extends MultiTenantCapable<T>, ID extends Serializable> 
extends SimpleJpaRepository<T, ID> implements JpaRepository<T, ID> {

    private static final Logger LOG = LoggerFactory.getLogger(MultiTenantRepositoryImpl.class);

    private final EntityManager em;

    public MultiTenantRepositoryImpl(Class<T> entityClass, EntityManager em) {
        super(entityClass, em);
        LOG.info("Creating an instance of MultitenantRepositoryImpl");
        this.em = em;
    }

    @Override
    public List<T> findAll() {
        LOG.info("findAll() method called");
        return getQuery(null, (Sort) null).getResultList();
    }

    @Override
    protected TypedQuery<T> getQuery(Specification<T> spec, Sort sort) {
        return getQuery(spec, getDomainClass(), sort);
    }

    @Override
    protected <S extends T> TypedQuery<S> getQuery(Specification<S> spec, Class<S> domainClass, Sort sort) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<S> query = builder.createQuery(domainClass);

        Root<S> root = applySpecificationToCriteria(spec, domainClass, query);
        query.select(root);

        if (sort != null) {
            query.orderBy(toOrders(sort, root, builder));
        }

        return applyRepositoryMethodMetadata(em.createQuery(query));
    }

    private <S, U extends T> Root<U> applySpecificationToCriteria(
    Specification<U> specification, 
    Class<U> domainClass, 
    CriteriaQuery<S> criteriaQuery) {
        Assert.notNull(domainClass, "Domain class must not be null!");
        Assert.notNull(criteriaQuery, "CriteriaQuery must not be null!");

        Root<U> root = criteriaQuery.from(domainClass);
        CriteriaBuilder builder = em.getCriteriaBuilder();
        
        Predicate nonTenantPredicate = null;
        if (specification != null) {
            nonTenantPredicate = specification.toPredicate(root, criteriaQuery, builder);
        }

        final Predicate tenantPredicate = builder.equal(root.get("tenant"), TenantContext.get());
        if (nonTenantPredicate != null) {
            criteriaQuery.where(tenantPredicate, nonTenantPredicate);
        } else {
            criteriaQuery.where(tenantPredicate);
        }

        return root;
    }

    private <S> TypedQuery<S> applyRepositoryMethodMetadata(TypedQuery<S> query) {
        if (super.getRepositoryMethodMetadata() == null) {
            return query;
        }

        LockModeType type = super.getRepositoryMethodMetadata().getLockModeType();
        TypedQuery<S> toReturn = type == null ? query : query.setLockMode(type);

        for (Entry<String, Object> hint : getQueryHints().entrySet()) {
            query.setHint(hint.getKey(), hint.getValue());
        }

        return toReturn;
    }
}
