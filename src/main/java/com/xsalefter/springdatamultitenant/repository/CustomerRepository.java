package com.xsalefter.springdatamultitenant.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xsalefter.springdatamultitenant.model.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
