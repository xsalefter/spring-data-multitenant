package com.xsalefter.springdatamultitenant.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.xsalefter.springdatamultitenant.repository.MultiTenantRepositoryImpl;

@Configuration
@EnableJpaRepositories(
    basePackages = "com.xsalefter.springdatamultitenant.repository",
    repositoryFactoryBeanClass = MultiTenantJpaRepositoryFactoryBean.class,
    repositoryBaseClass = MultiTenantRepositoryImpl.class
)
public class DatabaseConfiguration {
}
