package com.xsalefter.springdatamultitenant.config;

import java.io.Serializable;

import javax.persistence.EntityManager;

import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.JpaEntityInformationSupport;
import org.springframework.data.repository.core.EntityInformation;
import org.springframework.data.repository.core.RepositoryInformation;
import org.springframework.data.repository.core.RepositoryMetadata;
import org.springframework.data.repository.core.support.RepositoryFactorySupport;

import com.xsalefter.springdatamultitenant.model.MultiTenantCapable;
import com.xsalefter.springdatamultitenant.repository.MultiTenantRepositoryImpl;

public class MultiTenantJpaRepositoryFactory<T extends MultiTenantCapable<T>> 
extends RepositoryFactorySupport {

    private final EntityManager entityManager;

    public MultiTenantJpaRepositoryFactory(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    protected Object getTargetRepository(RepositoryInformation information) {
        @SuppressWarnings("unchecked")
        final Class<T> entityClass = (Class<T>) information.getDomainType();
        return new MultiTenantRepositoryImpl<>(entityClass, this.entityManager);
    }

    @SuppressWarnings({"unchecked", "hiding"})
    @Override
    public <T, ID extends Serializable> EntityInformation<T, ID> 
    getEntityInformation(Class<T> domainClass) {
        return (JpaEntityInformation<T, ID>) JpaEntityInformationSupport.getEntityInformation(domainClass, entityManager);
    }

    @Override
    protected Class<?> getRepositoryBaseClass(RepositoryMetadata metadata) {
        return MultiTenantRepositoryImpl.class;
    }
}
