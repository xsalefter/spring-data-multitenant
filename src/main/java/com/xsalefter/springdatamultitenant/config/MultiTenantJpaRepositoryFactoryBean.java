package com.xsalefter.springdatamultitenant.config;

import java.io.Serializable;

import javax.persistence.EntityManager;

import org.springframework.data.jpa.repository.support.JpaRepositoryFactoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.core.support.RepositoryFactorySupport;

import com.xsalefter.springdatamultitenant.model.MultiTenantCapable;

public class MultiTenantJpaRepositoryFactoryBean< 
    R extends PagingAndSortingRepository<T, I>, 
    T extends MultiTenantCapable<T>, 
    I extends Serializable> 
extends JpaRepositoryFactoryBean<R, T, I> {

    public MultiTenantJpaRepositoryFactoryBean(Class<? extends R> repositoryInterface) {
        super(repositoryInterface);
    }

    protected RepositoryFactorySupport createRepositoryFactory(EntityManager entityManager) {
        return new MultiTenantJpaRepositoryFactory<>(entityManager);
    }
}
