package com.xsalefter.springdatamultitenant;

import javax.persistence.EntityManager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

@SpringBootApplication
public class Application {

    public static void main(String...args) {
        final ConfigurableApplicationContext applicationContext = SpringApplication.run(Application.class, args);
        final EntityManager entityManager = applicationContext.getBean(EntityManager.class);
        final TransactionTemplate transactionTemplate = applicationContext.getBean(TransactionTemplate.class);
        transactionTemplate.execute(new TransactionCallbackWithoutResult() {
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                final DataInitializer dataInitializer = new DataInitializer(entityManager);
                dataInitializer.withMultipleTenants();
            }
        });
    }
}
