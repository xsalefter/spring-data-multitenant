package com.xsalefter.springdatamultitenant.model;

import java.io.Serializable;

public interface MultiTenantCapable<T> extends Serializable {
    T getTenant();
}
