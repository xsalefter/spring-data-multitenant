package com.xsalefter.springdatamultitenant.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.xsalefter.springdatamultitenant.TenantContext;
import com.xsalefter.springdatamultitenant.model.Customer;
import com.xsalefter.springdatamultitenant.model.Organization;

import static org.assertj.core.api.Assertions.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CustomerRepositoryTest {

    @Autowired
    private CustomerRepository customerRepository;

    @PersistenceContext
    private EntityManager entityManager;

    @Test
    public void findAll_thenReturnAll() {
        this.withDefaultTestData();

        Iterable<Customer> customers = this.customerRepository.findAll();
        assertThat(customers).isNotNull();
        assertThat(customers).isNotEmpty();
        assertThat(customers).hasSize(3);
        customers.forEach(c -> assertThat(c.getTenant()).isNotNull());
        customers.forEach(c -> assertThat(c.getTenant().getName()).isEqualTo("Lannister"));
    }

    @Test
    public void findAll_thenReturnOnlySpecificsTenant() {
        this.withMultipleTenants();

        Organization tenant = this.getOrganization("Stark");
        assertThat(tenant).isNotNull();
        assertThat(tenant.getName()).isEqualTo("Stark");

        TenantContext.set(tenant.getId());

        Iterable<Customer> customers = this.customerRepository.findAll();
        assertThat(customers).isNotNull();
        assertThat(customers).isNotEmpty();
        assertThat(customers).hasSize(4);
        customers.forEach(c -> assertThat(c.getTenant()).isNotNull());
        customers.forEach(c -> assertThat(c.getTenant().getName()).isEqualTo("Stark"));
    }

    private void withDefaultTestData() {
        Organization tenant = new Organization("Lannister");
        this.entityManager.persist(tenant);

        this.entityManager.persist(new Customer("Cercei", "Lannister", tenant));
        this.entityManager.persist(new Customer("Tyrion", "Lannister", tenant));
        this.entityManager.persist(new Customer("Jaime", "Lannister", tenant));
    }

    private void withMultipleTenants() {
        this.withDefaultTestData();

        this.entityManager.persist(new Organization("Stark"));
        final Organization exes = getOrganization("Stark");

        this.entityManager.persist(new Customer("Sansa", "Stark", exes));
        this.entityManager.persist(new Customer("John", "Snow", exes));
        this.entityManager.persist(new Customer("Catelyn", "Stark", exes));
        this.entityManager.persist(new Customer("Ned", "Stark", exes));
    }

    private Organization getOrganization(final String name) {
        return this.entityManager
            .createQuery("from Organization o where o.name = :name", Organization.class)
            .setParameter("name", name).getSingleResult();
    }
}
